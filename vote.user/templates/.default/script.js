document.loadVoteItem = function () {
    mixitup(document.querySelector('.container'), {
        'animation': {
            'duration': 0,
            'nudge': false,
            'reverseOut': false,
            'effects': 'fade'
        }
    });
    $('.controls button:first').click();
};

$(function () {
    'use strict';

    $(document).on('click', '.vote-item', function () {
        var mix_parant = $(this).closest('.mix');
        if (mix_parant.hasClass('active')) {
            $('html').animate({scrollTop: $(document).height()}, 1100);
        }
        mix_parant.addClass('active').siblings('.mix').removeClass('active');
        $(this).find('input').prop('checked', true);

    });
});