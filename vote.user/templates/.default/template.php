<? if (empty($arParams['WRAPPER']['IS_AJAX'])) { ?>
    <div class="ny_body_item ny_item">
        <div class="<?= $arParams['WRAPPER']["CONTANER"] ?> ny_item__main-container">
        <? } ?>
        <div class="ny_item__title"><?= $arResult['NAME'] ?></div>
        <form class="<?= $arParams['WRAPPER']['CONTANER_FORM'] ?>">

            <div class="controls ny_item__controls">
                <? if (isset($arResult['TABS'])) { ?>
                    <? foreach ($arResult['TABS'] as $key => $arTabs) { ?>
                        <button type="button" class="control" data-filter=".section-<?= $key ?>"><?= $arTabs['NAME'] ?></button>
                    <? } ?>
                <? } ?>
            </div>
            <div class="ny_item__container-item container">
                <? foreach ($arResult['USERS'] as $key => $arItem) { ?>
                    <div class="peoples_img mix ih-item circle colored effect3 left_to_right item<?= $key ?> <?= $arItem['SECTION'] ?>">
                        <a class="vote-item">
                            <input type="radio" name="user-vote" value="<?= $key ?>" class="input_hidden">
                            <div class="img">
                                <img src="<?= $arItem['PHOTO'] ?>" alt="<?= $arItem['NAME'] ?>">
                            </div>
                            <div class="ny_ok_block"></div>
                            <div class="info">
                                <div class="ny_item__info-text"><span class="default_text">Выбрать сотрудника</span><span class="accept_text">Подтвердить выбор</span></div>
                            </div>
                        </a>
                        <div class="ny_people_name"><?= $arItem['NAME'] ?></div>
                    </div>
                <? } ?>
            </div>
            <div class="center ny_float_submit">
                <button class="red_btn" type="submit">ПРОГОЛОСОВАТЬ</button>
            </div>
        </form>
        <script>
            document.loadVoteItem();
        </script>
        <? if (empty($arParams['WRAPPER']['IS_AJAX'])) { ?>
        </div>
    </div>
<? } ?>

