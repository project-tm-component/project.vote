<?php

CBitrixComponent::includeComponentClass("project.catalog:element");

use Bitrix\Main\Application,
    Project\Vote\Vote;

class projectVoteUser extends projectCatalogElement {

    public function executeComponent() {
        if (parent::executeComponent() and self::projectLoader('project.vote')) {
            if (Project\Vote\Vote::status($this->arResult['SECTION']['ID'], $this->arResult['ID'])) {
                LocalRedirect($this->arParams['WRAPPER']['PARAM']['SECTION_URL']);
            }

            $arFilter = array(
                'ACTIVE' => 'Y',
            );
            if ($this->arResult['PROPERTY_USER_VALUE']) {
                $arFilter['ID'] = implode('|', $this->arResult['PROPERTY_USER_VALUE']);
            } else {
                $this->arResult['TABS'] = array();
                foreach (Project\Vote\Config::DEPARTAMENT as $name => $list) {
                    $this->arResult['TABS'][] = array(
                        'NAME' => $name,
                        'LIST' => $list,
                    );
                }
            }
            $this->arResult['USERS'] = array();
            $rsUsers = CUser::GetList(($by = "name"), ($order = "asc"), $arFilter, array('FIELDS' => array('ID', 'LOGIN', 'NAME', 'LAST_NAME', 'SECOND_NAME', 'PERSONAL_PHOTO'), 'SELECT' => array('UF_DEPARTMENT')));
            while ($arItem = $rsUsers->Fetch()) {
//                if ($arItem['ID'] == cUser::getId() or empty($arItem['NAME']) or empty($arItem['PERSONAL_PHOTO']) or in_array($arItem['ID'], Project\Vote\Config::NO_USER)) {
                if ($arItem['ID'] == cUser::getId() or empty($arItem['NAME']) or in_array($arItem['ID'], Project\Vote\Config::NO_USER)) {
                    continue;
                }
                if(strpos($arItem['NAME'], 'Обучение')===0) {
                    continue;
                }
                $arUser = array(
                    'NAME' => $arItem['NAME'] . ' ' . $arItem['LAST_NAME'],
                    'PHOTO' => Project\Tools\Utility\Image::resize($arItem['PERSONAL_PHOTO'], 220, 220, BX_RESIZE_IMAGE_EXACT)
                );
                if(empty($arItem['PERSONAL_PHOTO'])) {
                    $arUser['PHOTO'] = '/local/templates/markup/dist/ihover/client/images/assets/1.jpg';
                }
                if (empty($this->arResult['PROPERTY_USER_VALUE'])) {
                    $section = array();
                    foreach ($arItem['UF_DEPARTMENT'] as $dep) {
                        foreach ($this->arResult['TABS'] as $key => $value) {
                            if (in_array($dep, $value['LIST'])) {
                                $section[] = 'section-' . $key;
                            }
                        }
                    }
                    if (empty($section)) {
                        continue;
                    }
                    $arUser['SECTION'] = implode(' ', $section);
                }
                $this->arResult['USERS'][$arItem['ID']] = $arUser;
            }

            if (!empty($this->arParams['WRAPPER']['IS_AJAX'])) {
                $request = Application::getInstance()->getContext()->getRequest();
                if ($request->getPost('user-vote')) {
                    Vote::add($this->arResult['SECTION']['ID'], $this->arResult['ID'], $request->getPost('user-vote'));
                    ?>
                    <script>
                        document.location.reload();
                    </script>
                    <?
                    return;
                }
            }
            $this->includeComponentTemplate();
        }
    }

    protected function projectTemplate() {
        
    }

}
